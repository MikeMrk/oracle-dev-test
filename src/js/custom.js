$(function(){
   var openMenu = function(){
       $('header.main-header').addClass('menu-active');
       $('body').addClass('no-scroll');
   };
    
    var closeMenu = function(){
        $('header.main-header').removeClass('menu-active');
        $('body').removeClass('no-scroll');
    }; 
    
    $('.toggle-nav').on('click', function(){
       openMenu(); 
    });
    
    $('.close-menu').on('click', function(){
       closeMenu(); 
    });
    
   
  
});

 //initialize masonry js
function loadGrid(){
    $('.grid').masonry({
  // options
   itemSelector: '.grid-item',
    percentPosition: true
  
    });  
};
$(window).on('load', function(){
    loadGrid();
    
});

//initialize lightgallery
$(document).ready(function() {
     $("#lightgallery").lightGallery(); 
});